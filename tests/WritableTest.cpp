#include <cstring>
#include <rili/Stream.hpp>
#include <rili/Test.hpp>
#include <string>
#include <tests/StreamMocks.hpp>

TYPED_TEST(Writable, isAbleToConvertNumericTypes) {
    TypeParam var = 1234;
    WritableMock cut;

    EXPECT_CALL(cut, write, [](char const* data, std::size_t size) {
        EXPECT_EQ(size, 4u);
        std::string d(data, data + size);
        EXPECT_EQ(d, "1234");
    });

    EXPECT_CALL(cut, flush, []() -> std::exception_ptr { return {}; });
    EXPECT_TRUE(cut << var << rili::stream::flush);

    EXPECT_CALL(cut, flush, []() { return std::make_exception_ptr(rili::stream::error::WritableEnd()); });
    EXPECT_FALSE(cut << rili::stream::flush);
}

PARAMETERIZE_TYPED_TEST(Writable, isAbleToConvertNumericTypes,
                        short,               // NOLINT (runtime/int)
                        unsigned short,      // NOLINT (runtime/int)
                        long,                // NOLINT (runtime/int)
                        unsigned long,       // NOLINT (runtime/int)
                        long long,           // NOLINT (runtime/int)
                        unsigned long long,  // NOLINT (runtime/int)
                        int, unsigned int, float, double)

TEST(Writable, isAbleToConvertNumericTypes_bool_true) {
    bool var = true;
    WritableMock cut;

    EXPECT_CALL(cut, write, [](char const* data, std::size_t size) {
        EXPECT_EQ(size, 1u);
        std::string d(data, data + size);
        EXPECT_EQ(d, "1");
    });

    EXPECT_CALL(cut, flush, []() -> std::exception_ptr { return {}; });
    EXPECT_TRUE(cut << var << rili::stream::flush);
}

TEST(Writable, isAbleToConvertNumericTypes_bool_false) {
    bool var = false;
    WritableMock cut;

    EXPECT_CALL(cut, write, [](char const* data, std::size_t size) {
        EXPECT_EQ(size, 1u);
        std::string d(data, data + size);
        EXPECT_EQ(d, "0");
    });

    EXPECT_CALL(cut, flush, []() -> std::exception_ptr { return {}; });
    EXPECT_TRUE(cut << var << rili::stream::flush);
}

TEST(Writable, isAbleToConvertNumericTypes_voidPtr) {
    std::uintptr_t var = 0x12345;
    WritableMock cut;

    EXPECT_CALL(cut, write, [](char const* data, std::size_t size) {
        EXPECT_EQ(size, 7u);
        std::string d(data, data + size);
        EXPECT_EQ(d, "0x12345");
    });

    EXPECT_CALL(cut, flush, []() -> std::exception_ptr { return {}; });
    EXPECT_TRUE(cut << reinterpret_cast<void*>(var) << rili::stream::flush);
}

TEST(Writable, isAbleToConvertString) {
    std::string var = "some string";
    WritableMock cut;

    EXPECT_CALL(cut, write, [&var](char const* data, std::size_t size) {
        EXPECT_EQ(size, var.size());
        std::string d(data, data + size);
        EXPECT_EQ(d, var);
    });

    EXPECT_CALL(cut, flush, []() -> std::exception_ptr { return {}; });
    EXPECT_TRUE(cut << var << rili::stream::flush);
}

TEST(Writable, isAbleToConvertCString) {
    const char* const var = "some string";
    WritableMock cut;

    EXPECT_CALL(cut, write, [&var](char const* data, std::size_t size) {
        EXPECT_EQ(size, std::strlen(var));
        std::string d(data, data + size);
        EXPECT_EQ(d, var);
    });

    EXPECT_CALL(cut, flush, []() -> std::exception_ptr { return {}; });
    EXPECT_TRUE(cut << var << rili::stream::flush);
}

TEST(Writable, isAbleToConvertUnsignedCString) {
    const char* const var = "some string";
    auto uvar = reinterpret_cast<const unsigned char* const>(var);
    WritableMock cut;

    EXPECT_CALL(cut, write, [&var](char const* data, std::size_t size) {
        EXPECT_EQ(size, std::strlen(var));
        std::string d(data, data + size);
        EXPECT_EQ(d, var);
    });

    EXPECT_CALL(cut, flush, []() -> std::exception_ptr { return {}; });
    EXPECT_TRUE(cut << uvar << rili::stream::flush);
}

TEST(Writable, isAbleToConvertChar) {
    char var = 'c';
    WritableMock cut;

    EXPECT_CALL(cut, write, [&var](char const* data, std::size_t size) {
        EXPECT_EQ(size, 1u);
        EXPECT_EQ(data[0], var);
    });

    EXPECT_CALL(cut, flush, []() -> std::exception_ptr { return {}; });
    EXPECT_TRUE(cut << var << rili::stream::flush);
}

TEST(Writable, isAbleToConvertUnsignedChar) {
    unsigned char var = 'c';
    WritableMock cut;

    EXPECT_CALL(cut, write, [&var](char const* data, std::size_t size) {
        EXPECT_EQ(size, 1u);
        EXPECT_EQ(data[0], var);
    });

    EXPECT_CALL(cut, flush, []() -> std::exception_ptr { return {}; });
    EXPECT_TRUE(cut << var << rili::stream::flush);
}

TEST(Writable, isAbleToWorkWithCustomFormater) {
    WritableMock cut;

    EXPECT_CALL(cut, write, [](char const* data, std::size_t size) {
        EXPECT_EQ(size, 3u);
        std::string d(data, data + size);
        EXPECT_EQ(d, "sth");
    });

    EXPECT_CALL(cut, flush, []() -> std::exception_ptr { return {}; });
    cut << std::function<rili::stream::Writable&(rili::stream::Writable&)>(
               [](rili::stream::Writable& w) -> rili::stream::Writable& {
                   w.write("sth", 3);
                   return w;
               })
        << rili::stream::flush;
}
