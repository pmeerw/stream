#include <rili/Stream.hpp>
#include <rili/Test.hpp>
#include <sstream>
#include <string>
#include <tests/StreamMocks.hpp>

TEST(StdReadable, basic) {
    std::stringstream ss("1234 some text 2.4 ");
    rili::stream::std::Readable cut(ss);

    int i;
    std::string s1, s2, s3;
    float f;
    EXPECT_TRUE(cut >> i);
    EXPECT_TRUE(cut >> s1);
    EXPECT_TRUE(cut >> s2);
    EXPECT_TRUE(cut >> f);

    EXPECT_EQ(1234, i);
    EXPECT_EQ("some", s1);
    EXPECT_EQ("text", s2);
    EXPECT_GT(f, 2.3);
    EXPECT_LT(f, 2.5);

    auto result = cut >> s3;
    EXPECT_FALSE(result);
    EXPECT_EQ(result.brief(), rili::stream::OperationResult::Brief::ReadableEnd);
}

TEST(StdWritable, basic) {
    std::stringstream ss;
    rili::stream::std::Writable cut(ss);
    EXPECT_TRUE(cut << 1234 << " some text " << 2.0 << " " << rili::stream::flush);

    EXPECT_EQ(ss.str(), "1234 some text 2 ");
}

TEST(WritableStreambuf, basic) {
    WritableMock mock;
    rili::stream::std::WritableStreambuf cut(mock);
    std::ostream os(&cut);

    std::string expected = "1234 some text 2 ";
    for (auto const& c : expected) {
        EXPECT_CALL(mock, write, [c](char const* data, std::size_t size) {
            EXPECT_EQ(size, 1u);
            EXPECT_EQ(*data, c);
        });
    }
    EXPECT_CALL(mock, flush, []() -> std::exception_ptr { return {}; });
    os << 1234 << " some text " << 2.0 << " " << std::flush;
    EXPECT_TRUE(os.good());

    EXPECT_CALL(mock, flush, []() { return std::make_exception_ptr(5); });
    os << std::flush;

    EXPECT_FALSE(os.good());
}

TEST(ReadableStreambuf, basic) {
    rili::stream::BufferingFifoDuplex stub;
    rili::stream::std::ReadableStreambuf cut(stub);
    std::istream is(&cut);
    stub << "1234 some text 2 " << rili::stream::flush;
    int i1 = 0;
    int i2 = 0;
    std::string s1;
    std::string s2;
    std::string s3;
    float f = 0;

    is >> i1 >> s1 >> s2 >> f;
    EXPECT_TRUE(is.good());
    is >> i2;
    EXPECT_FALSE(is.good());

    EXPECT_EQ(i1, 1234);
    EXPECT_EQ(s1, "some");
    EXPECT_EQ(s2, "text");
    EXPECT_GT(f, 1.9);
    EXPECT_LT(f, 2.1);

    is >> s3;
    EXPECT_FALSE(is.good());
}
