#pragma once
#include <rili/Stream.hpp>
#include <rili/Test.hpp>

class WritableMock final : public rili::stream::Writable {
 public:
    WritableMock() = default;
    WritableMock(WritableMock const&) = delete;
    WritableMock& operator=(WritableMock const&) = delete;
    virtual ~WritableMock() = default;

 public:
    MOCK0(std::exception_ptr, flush, )
    MOCK2(void, write, char const*, std::size_t, )
    MOCK0(rili::stream::view::Base const&, writableView, const)
};

class ReadableMock final : public rili::stream::Readable {
 public:
    ReadableMock() = default;
    ReadableMock(ReadableMock const&) = delete;
    ReadableMock& operator=(ReadableMock const&) = delete;
    virtual ~ReadableMock() = default;

 public:
    MOCK1(std::exception_ptr, pull, std::size_t, )
    MOCK1(void, consume, std::size_t, )
    rili::stream::view::Base const& readableView() const override { return m_view; }
    rili::stream::view::Default& view() { return m_view; }

 private:
    rili::stream::view::Default m_view;
};

class DuplexMock final : public rili::stream::Duplex {
 public:
    DuplexMock() = default;
    DuplexMock(DuplexMock const&) = delete;
    DuplexMock& operator=(DuplexMock const&) = delete;
    virtual ~DuplexMock() = default;

 public:
    MOCK0(std::exception_ptr, flush, )
    MOCK1(std::exception_ptr, pull, std::size_t, )
    MOCK1(void, consume, std::size_t, )
    MOCK2(void, write, char const*, std::size_t, )
    MOCK0(rili::stream::view::Base const&, readableView, const)
    MOCK0(rili::stream::view::Base const&, writableView, const)
};

class BufferingWritableMock final : public rili::stream::BufferingWritable {
 public:
    BufferingWritableMock() = default;
    BufferingWritableMock(BufferingWritableMock const&) = delete;
    BufferingWritableMock& operator=(BufferingWritableMock const&) = delete;
    virtual ~BufferingWritableMock() = default;

 public:
    MOCK1(std::exception_ptr, commit, rili::stream::view::Default&, )
};

class BufferingReadableMock final : public rili::stream::BufferingReadable {
 public:
    BufferingReadableMock() = default;
    BufferingReadableMock(BufferingReadableMock const&) = delete;
    BufferingReadableMock& operator=(BufferingReadableMock const&) = delete;
    virtual ~BufferingReadableMock() = default;

 public:
    MOCK1(std::exception_ptr, fetch, rili::stream::view::Default&, )
};
