#include <rili/Stream.hpp>
#include <rili/Test.hpp>

int main(int, char**) {
    rili::stream::cout() << "Running..." << rili::stream::endl;
    return rili::test::runner::run() ? 0 : 1;
}
