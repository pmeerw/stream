#pragma once
#include <rili/Stream.hpp>
#include <rili/Test.hpp>

class ViewMock : public rili::stream::view::Base {
 public:
    ViewMock() = default;
    ViewMock(ViewMock const&) = delete;
    ViewMock& operator=(ViewMock const&) = delete;
    virtual ~ViewMock() = default;

 public:
    MOCK0(char const*, data, const)
    MOCK0(std::size_t, size, const)
};
