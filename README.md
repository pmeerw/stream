# Rili Stream

This is module of [rili](https://gitlab.com/rilis/rili) which contain implementation of improved streams

**Documentation and API reference** :  [rilis.io](https://rilis.io/projects/rili/library/stream)

**[Issue tracker](https://gitlab.com/rilis/rilis/issues)**

**Project status**

[![build status](https://gitlab.com/rilis/rili/stream/badges/master/build.svg)](https://gitlab.com/rilis/rili/stream/commits/master)
[![coverage report](https://gitlab.com/rilis/rili/stream/badges/master/coverage.svg)](https://gitlab.com/rilis/rili/stream/commits/master)
