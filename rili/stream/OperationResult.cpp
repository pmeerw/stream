#include <rili/stream/OperationResult.hpp>

namespace rili {
namespace stream {
namespace error {

char const* WritableEnd::what() const noexcept {
    static char const* msg =
        "rili::stream::error::WritableEnd: operation failed because because can't flush more data.";
    return msg;
}

char const* ReadableEnd::what() const noexcept {
    static char const* msg = "rili::stream::error::WritableEnd: operation failed because because can't pull more data.";
    return msg;
}

char const* UnexpectedData::what() const noexcept {
    static char const* msg =
        "rili::stream::error::UnexpectedData: operation failed because data other than expected occured.";
    return msg;
}

char const* OutOfRange::what() const noexcept {
    static char const* msg =
        "rili::stream::error::OutOfRange: operation failed because provided type can't hold conversion result";
    return msg;
}

}  // namespace error
}  // namespace stream
}  // namespace rili
