#include <cctype>
#include <cerrno>
#include <cmath>
#include <cstdint>
#include <limits>
#include <rili/stream/Stream.hpp>
#include <string>
#include <vector>

namespace rili {
namespace stream {
const ::std::function<OperationResult(Writable&)> flush = [](Writable& w) {
    auto result = w.flush();
    if (result) {
        return OperationResult(OperationResult::Brief::WritableEnd, result);
    } else {
        return OperationResult();
    }
};
const ::std::function<OperationResult(Writable&)> endl = [](Writable& w) {
    static const char* const newline = "\n";
    w.write(newline, 1);
    return flush(w);
};

namespace {
template <typename ResultType, typename ConversionFunctionResultType>
OperationResult readerIntImpl(Readable& r, ResultType& v, ConversionFunctionResultType (*f)(const char*, char**, int),
                              int base) {
    Readable const& cr = r;
    char* end;
    ConversionFunctionResultType tmp = v;
    if (cr.readableView().empty()) {
        auto pullResult = r.pull(0);
        if (pullResult) {
            return OperationResult(OperationResult::Brief::ReadableEnd, pullResult);
        }
    }
    ::std::size_t beginPosition = 0;
    {
        for (;;) {
            if (beginPosition >= cr.readableView().size()) {
                auto pullResult = r.pull(0);
                if (pullResult) {
                    return OperationResult(OperationResult::Brief::ReadableEnd, pullResult);
                }
            } else if (!::std::isspace(cr.readableView()[beginPosition])) {
                break;
            } else {
                beginPosition++;
            }
        }
    }

    for (;;) {
        tmp = f(cr.readableView().data(), &end, base);
        if (end == cr.readableView().data()) {
            return OperationResult(OperationResult::Brief::UnexpectedData,
                                   ::std::make_exception_ptr(error::UnexpectedData()));
        } else if (tmp > ::std::numeric_limits<ResultType>::max() || tmp < ::std::numeric_limits<ResultType>::min()) {
            return OperationResult(OperationResult::Brief::OutOfRange, ::std::make_exception_ptr(error::OutOfRange()));
        } else if ((tmp == ::std::numeric_limits<ConversionFunctionResultType>::max() ||
                    tmp == ::std::numeric_limits<ConversionFunctionResultType>::min()) &&
                   ERANGE == errno) {
            return OperationResult(OperationResult::Brief::OutOfRange, ::std::make_exception_ptr(error::OutOfRange()));
        } else if (end == cr.readableView().data() + cr.readableView().size()) {
            auto pullResult = r.pull(0);
            if (pullResult) {
                return OperationResult(OperationResult::Brief::ReadableEnd, pullResult);
            }
        } else {
            auto const size = static_cast<::std::size_t>(end - cr.readableView().data());
            if (base == 16 && (size - beginPosition < 3)) {
                return OperationResult(OperationResult::Brief::OutOfRange,
                                       ::std::make_exception_ptr(error::OutOfRange()));
            } else {
                v = static_cast<ResultType>(tmp);
                r.consume(size);
                return {};
            }
        }
    }
}

OperationResult readerBoolImpl(Readable& r, bool& v) {
    Readable const& cr = r;
    char const* end;
    char tmp = v;
    if (cr.readableView().empty()) {
        auto pullResult = r.pull(0);
        if (pullResult) {
            return OperationResult(OperationResult::Brief::ReadableEnd, pullResult);
        }
    }

    {
        ::std::size_t beginPosition = 0;
        for (;;) {
            if (beginPosition >= cr.readableView().size()) {
                auto pullResult = r.pull(0);
                if (pullResult) {
                    return OperationResult(OperationResult::Brief::ReadableEnd, pullResult);
                }
            } else if (!::std::isspace(cr.readableView()[beginPosition])) {
                break;
            } else {
                beginPosition++;
            }
        }
    }

    auto strTob = [](char const* data, char const** end) -> char {
        *end = data;
        while (::std::isspace(*data)) {
            data++;
        }
        char result = *data;
        if (::std::isspace(*(data + 1)) || *(data + 1) == '\0') {
            *end = data + 1;
        }
        return result;
    };
    for (;;) {
        tmp = strTob(cr.readableView().data(), &end);
        if (end == cr.readableView().data()) {
            return OperationResult(OperationResult::Brief::UnexpectedData,
                                   ::std::make_exception_ptr(error::UnexpectedData()));
        } else if (tmp != '1' && tmp != '0') {
            return OperationResult(OperationResult::Brief::OutOfRange, ::std::make_exception_ptr(error::OutOfRange()));
        } else if (end == cr.readableView().data() + cr.readableView().size()) {
            auto pullResult = r.pull(0);
            if (pullResult) {
                return OperationResult(OperationResult::Brief::ReadableEnd, pullResult);
            }
        } else {
            auto const size = static_cast<::std::size_t>(end - cr.readableView().data());
            v = (tmp == '1');
            r.consume(size);
            return {};
        }
    }
}

template <typename ResultType>
OperationResult readerFloatImpl(Readable& r, ResultType& v, ResultType (*f)(const char*, char**)) {
    Readable const& cr = r;
    char* end;
    ResultType tmp = v;
    if (cr.readableView().empty()) {
        auto pullResult = r.pull(0);
        if (pullResult) {
            return OperationResult(OperationResult::Brief::ReadableEnd, pullResult);
        }
    }
    {
        ::std::size_t currentPosition = 0;
        for (;;) {
            if (currentPosition >= cr.readableView().size()) {
                auto pullResult = r.pull(0);
                if (pullResult) {
                    return OperationResult(OperationResult::Brief::ReadableEnd, pullResult);
                }
            } else if (!::std::isspace(cr.readableView()[currentPosition])) {
                break;
            } else {
                currentPosition++;
            }
        }
    }
    for (;;) {
        tmp = f(cr.readableView().data(), &end);
        if (end == cr.readableView().data()) {
            return OperationResult(OperationResult::Brief::UnexpectedData,
                                   ::std::make_exception_ptr(error::UnexpectedData()));
        } else if ((tmp > ::std::numeric_limits<ResultType>::max() ||
                    tmp < ::std::numeric_limits<ResultType>::lowest()) &&
                   ERANGE == errno) {
            return OperationResult(OperationResult::Brief::OutOfRange, ::std::make_exception_ptr(error::OutOfRange()));
        } else if (end == cr.readableView().data() + cr.readableView().size()) {
            auto pullResult = r.pull(0);
            if (pullResult) {
                return OperationResult(OperationResult::Brief::ReadableEnd, pullResult);
            }
        } else {
            v = tmp;
            r.consume(static_cast<::std::size_t>(end - cr.readableView().data()));
            return {};
        }
    }
}
}  // namespace

namespace formater {

::std::function<OperationResult(Readable&)> reader(short& v) {  // NOLINT (runtime/int)
    return [&v](Readable& r) -> OperationResult {
        return readerIntImpl<short, long>(r, v, ::std::strtol, 0);  // NOLINT (runtime/int)
    };
}

::std::function<OperationResult(Readable&)> reader(unsigned short& v) {  // NOLINT (runtime/int)
    return [&v](Readable& r) -> OperationResult {
        return readerIntImpl<unsigned short, unsigned long>(r, v, ::std::strtoul, 0);  // NOLINT (runtime/int)
    };
}

::std::function<OperationResult(Readable&)> reader(int& v) {
    return [&v](Readable& r) -> OperationResult {
        return readerIntImpl<int, long>(r, v, ::std::strtol, 0);  // NOLINT (runtime/int)
    };
}

::std::function<OperationResult(Readable&)> reader(unsigned int& v) {
    return [&v](Readable& r) -> OperationResult {
        return readerIntImpl<unsigned int, unsigned long>(r, v, ::std::strtoul, 0);  // NOLINT (runtime/int)
    };
}

::std::function<OperationResult(Readable&)> reader(long& v) {  // NOLINT (runtime/int)
    return [&v](Readable& r) -> OperationResult {
        return readerIntImpl<long, long>(r, v, ::std::strtol, 0);  // NOLINT (runtime/int)
    };
}

::std::function<OperationResult(Readable&)> reader(unsigned long& v) {  // NOLINT (runtime/int)
    return [&v](Readable& r) -> OperationResult {
        return readerIntImpl<unsigned long, unsigned long>(r, v, ::std::strtoul, 0);  // NOLINT (runtime/int)
    };
}

::std::function<OperationResult(Readable&)> reader(long long& v) {  // NOLINT (runtime/int)
    return [&v](Readable& r) -> OperationResult {
        return readerIntImpl<long long, long long>(r, v, ::std::strtoll, 0);  // NOLINT (runtime/int)
    };
}

::std::function<OperationResult(Readable&)> reader(unsigned long long& v) {  // NOLINT (runtime/int)
    return [&v](Readable& r) -> OperationResult {
        return readerIntImpl<unsigned long long, unsigned long long>(r, v, ::std::strtoull, 0);  // NOLINT (runtime/int)
    };
}

::std::function<OperationResult(Readable&)> reader(float& v) {
    return [&v](Readable& r) -> OperationResult { return readerFloatImpl<float>(r, v, ::std::strtof); };
}

::std::function<OperationResult(Readable&)> reader(double& v) {
    return [&v](Readable& r) -> OperationResult { return readerFloatImpl<double>(r, v, ::std::strtod); };
}

::std::function<OperationResult(Readable&)> reader(bool& v) {
    return [&v](Readable& r) -> OperationResult { return readerBoolImpl(r, v); };
}

::std::function<OperationResult(Readable&)> reader(void*& v) {
    return [&v](Readable& r) -> OperationResult {
        ::std::uintptr_t vptr = 0;
        auto result =
            readerIntImpl<::std::uintptr_t, unsigned long long>(r, vptr, ::std::strtoull, 16);  // NOLINT (runtime/int)
        v = reinterpret_cast<void*>(vptr);
        return result;
    };
}

::std::function<OperationResult(Readable&)> reader(::std::string& v) {
    return [&v](Readable& r) -> OperationResult {
        Readable const& cr = r;
        ::std::size_t currentPosition = 0;
        ::std::size_t stringBegin = 0;
        for (;;) {
            if (currentPosition >= cr.readableView().size()) {
                auto pullResult = r.pull(0);
                if (pullResult) {
                    return OperationResult(OperationResult::Brief::ReadableEnd, pullResult);
                }
            } else if (!::std::isspace(cr.readableView()[currentPosition])) {
                stringBegin = currentPosition;
                break;
            } else {
                currentPosition++;
            }
        }
        for (;;) {
            if (currentPosition >= cr.readableView().size()) {
                auto pullResult = r.pull(0);
                if (pullResult) {
                    return OperationResult(OperationResult::Brief::ReadableEnd, pullResult);
                }
            } else if (::std::isspace(cr.readableView()[currentPosition])) {
                v = ::std::string(
                    ::std::next(
                        cr.readableView().data(),
                        static_cast<typename ::std::iterator_traits<const char*>::difference_type>(stringBegin)),
                    ::std::next(
                        cr.readableView().data(),
                        static_cast<typename ::std::iterator_traits<const char*>::difference_type>(currentPosition)));
                r.consume(currentPosition);
                return {};

            } else {
                currentPosition++;
            }
        }
    };
}

::std::function<Writable&(Writable&)> writer(short v) {  // NOLINT (runtime/int)
    return [v](Writable& w) -> Writable& {
        auto const result = ::std::to_string(static_cast<int>(v));
        w.write(result.data(), result.size());
        return w;
    };
}

::std::function<Writable&(Writable&)> writer(unsigned short v) {  // NOLINT (runtime/int)
    return [v](Writable& w) -> Writable& {
        auto const result = ::std::to_string(static_cast<unsigned int>(v));
        w.write(result.data(), result.size());
        return w;
    };
}
::std::function<Writable&(Writable&)> writer(int v) {
    return [v](Writable& w) -> Writable& {
        auto const result = ::std::to_string(v);
        w.write(result.data(), result.size());
        return w;
    };
}
::std::function<Writable&(Writable&)> writer(unsigned int v) {
    return [v](Writable& w) -> Writable& {
        auto const result = ::std::to_string(v);
        w.write(result.data(), result.size());
        return w;
    };
}
::std::function<Writable&(Writable&)> writer(long v) {  // NOLINT (runtime/int)
    return [v](Writable& w) -> Writable& {
        auto const result = ::std::to_string(v);
        w.write(result.data(), result.size());
        return w;
    };
}
::std::function<Writable&(Writable&)> writer(unsigned long v) {  // NOLINT (runtime/int)
    return [v](Writable& w) -> Writable& {
        auto const result = ::std::to_string(v);
        w.write(result.data(), result.size());
        return w;
    };
}
::std::function<Writable&(Writable&)> writer(long long v) {  // NOLINT (runtime/int)
    return [v](Writable& w) -> Writable& {
        auto const result = ::std::to_string(v);
        w.write(result.data(), result.size());
        return w;
    };
}
::std::function<Writable&(Writable&)> writer(unsigned long long v) {  // NOLINT (runtime/int)
    return [v](Writable& w) -> Writable& {
        auto const result = ::std::to_string(v);
        w.write(result.data(), result.size());
        return w;
    };
}
::std::function<Writable&(Writable&)> writer(float v) {
    return [v](Writable& w) -> Writable& {
        char buf[17] = {0};
        snprintf(buf, sizeof(buf), "%.9g", static_cast<double>(v));
        ::std::string s(buf);
        w.write(s.data(), s.size());
        return w;
    };
}
::std::function<Writable&(Writable&)> writer(double v) {
    return [v](Writable& w) -> Writable& {
        char buf[26] = {0};
        snprintf(buf, sizeof(buf), "%.17g", v);
        ::std::string s(buf);
        w.write(s.data(), s.size());
        return w;
    };
}

::std::function<Writable&(Writable&)> writer(bool v) {
    return [v](Writable& w) -> Writable& {
        char c = v ? '1' : '0';
        w.write(&c, 1);
        return w;
    };
}

::std::function<Writable&(Writable&)> writer(void* v) {
    return [v](Writable& w) -> Writable& {
        char buf[20] = {0};
        snprintf(buf, sizeof(buf), "0x%llx", reinterpret_cast<unsigned long long>(v));  // NOLINT (runtime/int)
        ::std::string s(buf);
        w.write(s.data(), s.size());
        return w;
    };
}
}  // namespace formater
}  // namespace stream
}  // namespace rili
