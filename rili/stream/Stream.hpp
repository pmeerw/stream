#pragma once
#include <algorithm>
#include <cstddef>
#include <functional>
#include <iterator>
#include <rili/stream/OperationResult.hpp>
#include <rili/stream/View.hpp>
#include <string>

namespace rili {
namespace stream {
class Readable;
class Writable;
typedef ::std::function<OperationResult(Readable&)> Reader;
typedef ::std::function<Writable&(Writable&)> Writer;

namespace formater {
/// @cond INTERNAL
extern Reader reader(short& v);               // NOLINT (runtime/int)
extern Reader reader(unsigned short& v);      // NOLINT (runtime/int)
extern Reader reader(long& v);                // NOLINT (runtime/int)
extern Reader reader(unsigned long& v);       // NOLINT (runtime/int)
extern Reader reader(long long& v);           // NOLINT (runtime/int)
extern Reader reader(unsigned long long& v);  // NOLINT (runtime/int)
extern Reader reader(int& v);
extern Reader reader(unsigned int& v);
extern Reader reader(float& v);
extern Reader reader(double& v);
extern Reader reader(bool& v);
extern Reader reader(void*& v);
extern Reader reader(::std::string& v);

extern Writer writer(short v);               // NOLINT (runtime/int)
extern Writer writer(unsigned short v);      // NOLINT (runtime/int)
extern Writer writer(long v);                // NOLINT (runtime/int)
extern Writer writer(unsigned long v);       // NOLINT (runtime/int)
extern Writer writer(long long v);           // NOLINT (runtime/int)
extern Writer writer(unsigned long long v);  // NOLINT (runtime/int)
extern Writer writer(int v);
extern Writer writer(unsigned int v);
extern Writer writer(float v);
extern Writer writer(double v);
extern Writer writer(bool v);
extern Writer writer(void* v);
/// @endcond INTERNAL
}  // namespace formater

/**
 * @brief The Writable class represent abstraction for writable stream sequence
 */
class Writable {
 private:
    Writable(Writable const&) = delete;
    Writable& operator=(Writable const&) = delete;

 public:
    inline Writable() = default;
    virtual ~Writable() = default;

 protected:
    /**
     * @brief Writable
     * @param other
     */
    Writable(Writable&& other) = default;
    /**
     * @brief operator =
     * @param other
     * @return
     */
    Writable& operator=(Writable&& other) = default;

 public:
    /**
     * @brief write used to write sequence of chars to stream of given size
     * @param data data which should be written
     * @param size number of chars to write
     */
    virtual void write(char const* data, ::std::size_t size) = 0;

    /**
     * @brief flush used to force move data to underlying sink
     * @return error exception if occured
     */
    virtual ::std::exception_ptr flush() = 0;

    /**
     * @brief view used to access currently avaliable data in stream
     * @return view
     */
    virtual view::Base const& writableView() const = 0;

 public:
    /**
     * @brief write used to write sequence of unsigned chars to stream of given size
     * @param data data which should be written
     * @param size number of unsigned chars to write
     */
    inline void write(unsigned char const* data, ::std::size_t size) {
        write(reinterpret_cast<char const*>(data), size);
    }

    /**
     * @brief operator << used to write single char to stream
     * @param v variable to be converted
     * @return stream
     */
    inline Writable& operator<<(char v) {
        write(&v, 1);
        return *this;
    }

    /**
     * @brief operator << used to write single unsigned char to stream
     * @param v variable to be converted
     * @return stream
     */
    inline Writable& operator<<(unsigned char v) {
        write(&v, 1);
        return *this;
    }

    /**
     * @brief operator << used to write C-string to stream
     * @param v array to be converted
     * @return stream
     */
    inline Writable& operator<<(char const* v) {
        ::std::string tmp(v);
        return *this << tmp;
    }

    /**
     * @brief operator << used to write C-string to stream
     * @param v array to be converted
     * @return stream
     */
    inline Writable& operator<<(unsigned char const* v) {
        ::std::string tmp(reinterpret_cast<char const*>(v));
        return *this << tmp;
    }

    /**
     * @brief operator << used to write std::string to stream
     * @param v array to be converted
     * @return stream
     */
    inline Writable& operator<<(::std::string const& v) {
        write(v.data(), v.size());
        return *this;
    }

    /**
     * @brief operator << used to write(convert) single short variable to stream
     * @param v variable to be converted
     * @return stream
     */
    inline Writable& operator<<(short v) {  // NOLINT (runtime/int)
        return formater::writer(v)(*this);
    }

    /**
     * @brief operator << used to write(convert) single unsigned short variable to stream
     * @param v variable to be converted
     * @return stream
     */
    inline Writable& operator<<(unsigned short v) {  // NOLINT (runtime/int)
        return formater::writer(v)(*this);
    }

    /**
     * @brief operator << used to write(convert) single long variable to stream
     * @param v variable to be converted
     * @return stream
     */
    inline Writable& operator<<(long v) {  // NOLINT (runtime/int)
        return formater::writer(v)(*this);
    }

    /**
     * @brief operator << used to write(convert) single unsigned long variable to stream
     * @param v variable to be converted
     * @return stream
     */
    inline Writable& operator<<(unsigned long v) {  // NOLINT (runtime/int)
        return formater::writer(v)(*this);
    }

    /**
     * @brief operator << used to write(convert) single long long variable to stream
     * @param v variable to be converted
     * @return stream
     */
    inline Writable& operator<<(long long v) {  // NOLINT (runtime/int)
        return formater::writer(v)(*this);
    }

    /**
     * @brief operator << used to write(convert) single unsigned long long variable to stream
     * @param v variable to be converted
     * @return stream
     */
    inline Writable& operator<<(unsigned long long v) {  // NOLINT (runtime/int)
        return formater::writer(v)(*this);
    }

    /**
     * @brief operator << used to write(convert) single int variable to stream
     * @param v variable to be converted
     * @return stream
     */
    inline Writable& operator<<(int v) { return formater::writer(v)(*this); }

    /**
     * @brief operator << used to write(convert) single unsigned int variable to stream
     * @param v variable to be converted
     * @return stream
     */
    inline Writable& operator<<(unsigned int v) { return formater::writer(v)(*this); }

    /**
     * @brief operator << used to write(convert) single float variable to stream
     * @param v variable to be converted
     * @return stream
     */
    inline Writable& operator<<(float v) { return formater::writer(v)(*this); }

    /**
     * @brief operator << used to write(convert) single double variable to stream
     * @param v variable to be converted
     * @return stream
     */
    inline Writable& operator<<(double v) { return formater::writer(v)(*this); }

    /**
     * @brief operator << used to write(convert) single bool variable to stream
     * @param v variable to be converted
     * @return stream
     */
    inline Writable& operator<<(bool v) { return formater::writer(v)(*this); }

    /**
     * @brief operator << used to write(convert) single pointer variable to stream
     * @param v variable to be converted
     * @return stream
     */
    inline Writable& operator<<(void* v) { return formater::writer(v)(*this); }

    /**
     * @brief operator << used to apply given function on stream
     * @param fn function to apply
     * @return operation result returned from function
     */
    inline OperationResult operator<<(::std::function<OperationResult(Writable&)> const& fn) { return fn(*this); }

    /**
     * @brief operator << used to apply given function on stream
     * @param fn function to apply
     * @return stream
     */
    inline Writable& operator<<(Writer const& fn) { return fn(*this); }
};

/**
 * @brief The Readable class represent abstraction for readable stream sequence
 */
class Readable {
 private:
    Readable(Readable const&) = delete;
    Readable& operator=(Readable const&) = delete;

 public:
    inline Readable() = default;
    virtual ~Readable() = default;

 protected:
    /**
     * @brief Readable
     * @param other
     */
    Readable(Readable&& other) = default;
    /**
     * @brief operator =
     * @param other
     * @return
     */
    Readable& operator=(Readable&& other) = default;

 public:
    /**
     * @brief read is used to read from stream given amount of data
     * @param data address where data should be readed from stream
     * @param size number of chars to read
     *
     * @note user must ensure that given number of chars is avaliable in stream
     */
    inline void read(unsigned char* data, ::std::size_t size) { read(reinterpret_cast<char*>(data), size); }

    /**
     * @brief read is used to read from stream given amount of data
     * @param data address where data should be readed from stream
     * @param size number of chars to read
     *
     * @note user must ensure that given number of chars is avaliable in stream
     */
    inline void read(char* data, ::std::size_t size) {
        auto const& d = readableView();
        for (::std::size_t i = 0; i < size; i++) {
            data[i] = d[i];
        }
        consume(size);
    }

    /**
     * @brief read read single byte to given variable
     * @param c place wher to read data
     *
     * @note you need previousely ensure stream view is not empty
     */
    inline void read(unsigned char& c) { return read(reinterpret_cast<char*>(&c), 1); }

    /**
     * @brief read read single byte to given variable
     * @param c place wher to read data
     *
     * @note you need previousely ensure stream view is not empty
     */
    inline void read(char& c) { return read(&c, 1); }

    /**
     * @brief consume remove given number of bytes from front of view
     * @param size number of bytes to remove
     */
    virtual void consume(::std::size_t size) = 0;

    /**
     * @brief readableView used to access stream view
     * @return view
     */
    virtual view::Base const& readableView() const = 0;

    /**
     * @brief pull used to pull from stream underlying sink at least given number of bytes
     * @param count number of bytes to retrieve from sink
     * @return error exception if occured
     *
     * @note implementation of this function do not should provide at least count number of bytes to view.
     * @note count equal to 0 means implementation should provide zero or more bytes to view and implementation should
     * choose easiest amount to provide.
     */
    virtual ::std::exception_ptr pull(::std::size_t count) = 0;

 public:
    /**
     * @brief operator >> read single character to given variable from stream
     * @param v variable where data should be stored
     * @return result of operation
     */
    inline OperationResult operator>>(char& v) {
        if (readableView().empty()) {
            auto pullResult = pull(1);
            if (pullResult) {
                return OperationResult(OperationResult::Brief::ReadableEnd, pullResult);
            }
        }
        read(v);
        return {};
    }

    /**
     * @brief operator >> read single character to given variable from stream
     * @param v variable where data should be stored
     * @return result of operation
     */
    inline OperationResult operator>>(unsigned char& v) {
        if (readableView().empty()) {
            auto pullResult = pull(1);
            if (pullResult) {
                return OperationResult(OperationResult::Brief::ReadableEnd, pullResult);
            }
        }
        read(v);
        return {};
    }

    /**
     * @brief operator >> read(convert) single short to given variable from stream
     * @param v variable where data should be stored
     * @return result of operation
     */
    inline OperationResult operator>>(short& v) {  // NOLINT (runtime/int)
        return formater::reader(v)(*this);
    }

    /**
     * @brief operator >> read(convert) single unsigned short to given variable from stream
     * @param v variable where data should be stored
     * @return result of operation
     */
    inline OperationResult operator>>(unsigned short& v) {  // NOLINT (runtime/int)
        return formater::reader(v)(*this);
    }

    /**
     * @brief operator >> read(convert) single long to given variable from stream
     * @param v variable where data should be stored
     * @return result of operation
     */
    inline OperationResult operator>>(long& v) {  // NOLINT (runtime/int)
        return formater::reader(v)(*this);
    }

    /**
     * @brief operator >> read(convert) single unsigned long to given variable from stream
     * @param v variable where data should be stored
     * @return result of operation
     */
    inline OperationResult operator>>(unsigned long& v) {  // NOLINT (runtime/int)
        return formater::reader(v)(*this);
    }

    /**
     * @brief operator >> read(convert) single long long to given variable from stream
     * @param v variable where data should be stored
     * @return result of operation
     */
    inline OperationResult operator>>(long long& v) {  // NOLINT (runtime/int)
        return formater::reader(v)(*this);
    }

    /**
     * @brief operator >> read(convert) single unsigned long long to given variable from stream
     * @param v variable where data should be stored
     * @return result of operation
     */
    inline OperationResult operator>>(unsigned long long& v) {  // NOLINT (runtime/int)
        return formater::reader(v)(*this);
    }

    /**
     * @brief operator >> read(convert) single int to given variable from stream
     * @param v variable where data should be stored
     * @return result of operation
     */
    inline OperationResult operator>>(int& v) { return formater::reader(v)(*this); }

    /**
     * @brief operator >> read(convert) single unsigned int to given variable from stream
     * @param v variable where data should be stored
     * @return result of operation
     */
    inline OperationResult operator>>(unsigned int& v) { return formater::reader(v)(*this); }

    /**
     * @brief operator >> read(convert) single float to given variable from stream
     * @param v variable where data should be stored
     * @return result of operation
     */
    inline OperationResult operator>>(float& v) { return formater::reader(v)(*this); }

    /**
     * @brief operator >> read(convert) single double to given variable from stream
     * @param v variable where data should be stored
     * @return result of operation
     */
    inline OperationResult operator>>(double& v) { return formater::reader(v)(*this); }

    /**
     * @brief operator >> read(convert) single bool to given variable from stream
     * @param v variable where data should be stored
     * @return result of operation
     */
    inline OperationResult operator>>(bool& v) { return formater::reader(v)(*this); }

    /**
     * @brief operator >> read(convert) single pointer value to given variable from stream
     * @param v variable where data should be stored
     * @return result of operation
     */
    inline OperationResult operator>>(void*& v) { return formater::reader(v)(*this); }

    /**
     * @brief operator >> read(convert) single std::string to given variable from stream
     * @param v variable where data should be stored
     * @return result of operation
     *
     * @note read to first whitespace occurance
     */
    inline OperationResult operator>>(::std::string& v) { return formater::reader(v)(*this); }

    /**
     * @brief operator >> apply given function on stream and return it result
     * @param fn function to apply
     * @return
     */
    inline OperationResult operator>>(Reader const& fn) { return fn(*this); }
};

/**
 * @brief The Duplex class represent abstraction for both readable and writable stream.
 *
 * @note relation of input and output is specialization defined
 */
class Duplex : public Readable, public Writable {
 public:
    inline Duplex() = default;
    virtual ~Duplex() = default;

 protected:
    /**
     * @brief Duplex
     * @param other
     */
    inline Duplex(Duplex&& other) = default;
    /**
     * @brief operator =
     * @param other
     * @return
     */
    inline Duplex& operator=(Duplex&& other) = default;
};

/**
 * @brief The WritableIterator class give similar functionality like std::ostream_iterator but work with rili
 * Writable streams instead std::ostream and utilize default Writable stream serializer for given type.
 */
template <typename T>
class WritableIterator : public ::std::iterator<::std::output_iterator_tag, void, void, void, void> {
 public:
    WritableIterator() = delete;

    /**
     * @brief WritableIterator
     * @param other
     */
    WritableIterator(WritableIterator const& other) = default;

    /**
     * @brief operator =
     * @param other
     * @return
     */
    WritableIterator& operator=(WritableIterator const& other) = default;
    ~WritableIterator() = default;

 public:
    /**
     * @brief WritableIterator construct iterator which will use writable stream to write data and default Writable
     * writers to write elements to writable
     * @param writable - stream to use
     */
    inline explicit WritableIterator(Writable& writable) : m_writable(&writable) {}

 public:
    /**
     * @brief operator = try write next value to stream using default Writer
     * @param value to convert and write
     * @return
     */
    WritableIterator& operator=(T const& value) {
        *m_writable << value;
        return *this;
    }

    /**
     * @brief operator * actually does not dereference the iterator other than to be assigned a value with operator=
     * @return *this
     */
    inline WritableIterator& operator*() { return *this; }

    /**
     * @brief operator ++ This function has no required effect, neither in the iterator nor in its associated stream
     * (streams advance automatically on insertion).
     * @return *this
     */
    inline WritableIterator& operator++() { return *this; }

    /**
     * @brief operator ++ This function has no required effect, neither in the iterator nor in its associated stream
     * (streams advance automatically on insertion).
     * @return *this
     */
    inline WritableIterator& operator++(int) { return *this; }

 private:
    Writable* m_writable;
};

/**
 * @brief The WritableCustomIterator class give similar functionality like std::ostream_iterator but work with rili
 * Writable streams instead std::ostream and utilize custom Writer for given type.
 */
template <typename T>
class WritableCustomIterator : public ::std::iterator<::std::output_iterator_tag, void, void, void, void> {
 public:
    WritableCustomIterator() = delete;
    /**
     * @brief WritableCustomIterator
     * @param other
     */
    WritableCustomIterator(WritableCustomIterator const& other) = default;

    /**
     * @brief operator =
     * @param other
     * @return
     */
    WritableCustomIterator& operator=(WritableCustomIterator const& other) = default;
    ~WritableCustomIterator() = default;

 public:
    /**
     * @brief WritableCustomIterator construct iterator which will use writable stream to write data and given generator
     * to
     * write elements to writable
     * @param writable - stream to use
     * @param generator - function to use to write elements to stream
     */
    inline WritableCustomIterator(Writable& writable, ::std::function<Writer(T const&)> const& generator)
        : m_writable(&writable), m_generator(generator) {}

 public:
    /**
     * @brief operator = try write next value to stream using given generator
     * @param value to convert and write
     * @return
     */
    WritableCustomIterator& operator=(T const& value) {
        *m_writable << m_generator(value);
        return *this;
    }

    /**
     * @brief operator * actually does not dereference the iterator other than to be assigned a value with operator=
     * @return *this
     */
    inline WritableCustomIterator& operator*() { return *this; }

    /**
     * @brief operator ++ This function has no required effect, neither in the iterator nor in its associated stream
     * (streams advance automatically on insertion).
     * @return *this
     */
    inline WritableCustomIterator& operator++() { return *this; }

    /**
     * @brief operator ++ This function has no required effect, neither in the iterator nor in its associated stream
     * (streams advance automatically on insertion).
     * @return *this
     */
    inline WritableCustomIterator& operator++(int) { return *this; }

 private:
    Writable* m_writable;
    ::std::function<Writer(T const&)> const& m_generator;
};

/**
 * @brief The ReadableIterator class give similar functionality like std::istream_iterator but work with rili
 * Readable streams instead std::istream and utilize default Readable stream serializer for given type.
 */
template <typename T>
class ReadableIterator : public ::std::iterator<::std::input_iterator_tag, T, void, const T*, const T&> {
 public:
    /**
     * @brief ReadableIterator create iterator which can be used as end() of stream
     */
    inline ReadableIterator() : m_readable(nullptr), m_value() {}

    /**
     * @brief ReadableIterator
     * @param other
     */
    ReadableIterator(ReadableIterator const& other) = default;

    /**
     * @brief operator =
     * @param other
     * @return
     */
    ReadableIterator& operator=(ReadableIterator const& other) = default;
    ~ReadableIterator() = default;

 public:
    /**
     * @brief operator == check if iterator operate on the same readable stream object
     * @param other
     * @return
     */
    inline bool operator==(ReadableIterator const& other) const { return m_readable == other.m_readable; }

    /**
     * @brief operator != check if iterator operate on different readable stream object
     * @param other
     * @return
     */
    inline bool operator!=(ReadableIterator const& other) const { return m_readable != other.m_readable; }

    /**
     * @brief ReadableIterator construct iterator which will use readable stream to read data and default Readable
     * readers to featch elements from readable
     * @param readable - stream to use
     */
    inline explicit ReadableIterator(Readable& readable) : m_readable(&readable), m_value() { ++*this; }

 public:
    /**
     * @brief operator * give access to current iterator value
     * @return value
     */
    inline const T& operator*() const { return m_value; }

    /**
     * @brief operator -> give access to current iterator value pointer
     * @return pointer to value
     */
    inline const T* operator->() const { return &m_value; }

    /**
     * @brief operator ++ try fetch next value from stream
     * @return
     */
    inline ReadableIterator& operator++() {
        if (m_readable && !(*m_readable >> m_value)) {
            m_readable = nullptr;
        }
        return *this;
    }

    /**
     * @brief operator ++ try fetch next value from stream
     * @return
     */
    inline ReadableIterator operator++(int) {
        auto tmp = *this;
        ++*this;
        return tmp;
    }

 private:
    Readable* m_readable;
    T m_value;
};

/**
 * @brief The ReadableCustomIterator class give similar functionality like std::istream_iterator but work with rili
 * Readable streams instead std::istream and utilize custom Reader for given type.
 */
template <typename T>
class ReadableCustomIterator : public ::std::iterator<::std::input_iterator_tag, T, void, const T*, const T&> {
 public:
    /**
     * @brief ReadableCustomIterator create iterator which can be used as end() of stream
     */
    inline ReadableCustomIterator() : m_readable(nullptr), m_value(), m_generator(nullptr) {}

    /**
     * @brief ReadableCustomIterator
     * @param other
     */
    ReadableCustomIterator(ReadableCustomIterator const& other) = default;

    /**
     * @brief operator =
     * @param other
     * @return
     */
    ReadableCustomIterator& operator=(ReadableCustomIterator const& other) = default;
    ~ReadableCustomIterator() = default;

 public:
    /**
     * @brief operator == check if iterator operate on the same readable stream object
     * @param other
     * @return
     */
    inline bool operator==(ReadableCustomIterator const& other) const { return m_readable == other.m_readable; }

    /**
     * @brief operator != check if iterator operate on different readable stream object
     * @param other
     * @return
     */
    inline bool operator!=(ReadableCustomIterator const& other) const { return m_readable != other.m_readable; }

    /**
     * @brief ReadableCustomIterator construct iterator which will use readable stream to read data and generator to
     * featch
     * elements from readable
     * @param readable - stream to use
     * @param generator - function to fetch elements from stream
     */
    inline ReadableCustomIterator(Readable& readable, ::std::function<Reader(T&)> const& generator)
        : m_readable(&readable), m_value(), m_generator(&generator) {
        ++*this;
    }

 public:
    /**
     * @brief operator * give access to current iterator value fetched by generator
     * @return value
     */

    inline const T& operator*() const { return m_value; }

    /**
     * @brief operator -> give access to current iterator value pointer fetched by generator
     * @return pointer to value
     */
    inline const T* operator->() const { return &m_value; }

    /**
     * @brief operator ++ try fetch next value from stream using generator
     * @return
     */
    inline ReadableCustomIterator& operator++() {
        if (m_readable && !(*m_readable >> (*m_generator)(m_value))) {
            m_readable = nullptr;
        }
        return *this;
    }

    /**
     * @brief operator ++ try fetch next value from stream using generator
     * @return
     */
    inline ReadableCustomIterator operator++(int) {
        auto tmp = *this;
        ++*this;
        return tmp;
    }

 private:
    Readable* m_readable;
    T m_value;
    ::std::function<Reader(T&)> const* m_generator;
};

/**
 * @brief flush call flush on given Writable stream
 */
extern const ::std::function<OperationResult(Writable&)> flush;

/**
 * @brief endl write '\n' to given writable stream and call flush on it
 */
extern const ::std::function<OperationResult(Writable&)> endl;

}  // namespace stream
}  // namespace rili
