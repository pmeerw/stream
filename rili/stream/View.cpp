#include <rili/stream/View.hpp>

namespace rili {
namespace stream {
namespace view {
const char* Default::data() const { return m_buffer.data(); }
::std::size_t Default::size() const { return m_buffer.size(); }
Base::~Base() = default;
}  // namespace view
}  // namespace stream
}  // namespace rili
