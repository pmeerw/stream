#include <algorithm>
#include <rili/stream/BufferingStream.hpp>

namespace rili {
namespace stream {
::std::exception_ptr BufferingWritable::flush() {
    do {
        auto commitResult = commit(m_view);
        if (commitResult) {
            return commitResult;
        }
    } while (!m_view.empty());
    return {};
}

const view::Base& BufferingWritable::writableView() const { return m_view; }

void BufferingWritable::write(const char* data, ::std::size_t size) {
    m_view.raw().insert(m_view.raw().size(), data, size);
}

void BufferingReadable::consume(::std::size_t size) {
    m_view.raw().erase(m_view.raw().begin(),
                       ::std::next(m_view.raw().begin(),
                                   static_cast<rili::stream::view::Default::container_type::difference_type>(size)));
}

const view::Base& BufferingReadable::readableView() const { return m_view; }

::std::exception_ptr BufferingReadable::pull(::std::size_t count) {
    ::std::size_t requiredMinimalBufferSize = m_view.size() + count;
    do {
        auto fetchResult = fetch(m_view);
        if (fetchResult) {
            return fetchResult;
        } else if (m_view.size() >= requiredMinimalBufferSize) {
            return {};
        }
    } while (true);
}

void BufferingFifoDuplex::consume(::std::size_t size) {
    m_view.raw().erase(m_view.raw().begin(),
                       ::std::next(m_view.raw().begin(),
                                   static_cast<rili::stream::view::Default::container_type::difference_type>(size)));
}

view::Base const& BufferingFifoDuplex::readableView() const { return m_view; }
view::Base const& BufferingFifoDuplex::writableView() const { return m_view; }
::std::exception_ptr BufferingFifoDuplex::pull(::std::size_t /*count*/) {
    return ::std::make_exception_ptr(error::ReadableEnd());
}
void BufferingFifoDuplex::write(char const* data, ::std::size_t size) {
    m_view.raw().insert(m_view.raw().size(), data, size);
}
::std::exception_ptr BufferingFifoDuplex::flush() { return {}; }
}  // namespace stream
}  // namespace rili
